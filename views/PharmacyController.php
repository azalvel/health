<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\HealthDrug;

class PharmacyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $listing = HealthDrug::all();

        return view('pharmacy.index', compact('listing'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pharmacy.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'first_name'=>'required',
            'last_name'=>'required',
            'email'=>'required'
        ]);

        $contact = new HealthDrug([
            'first_name' => $request->get('first_name'),
            'last_name' => $request->get('last_name'),
            'email' => $request->get('email'),
            'job_title' => $request->get('job_title'),
            'city' => $request->get('city'),
            'country' => $request->get('country')
        ]);
        $contact->save();
        return redirect('/medcine')->with('success', 'Medicament saved!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $record = HealthDrug::find($id);

        return view('pharmacy.show', compact('record'));   
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $record = HealthDrug::find($id);

        return view('pharmacy.edit', compact('record'));   
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'first_name'=>'required',
            'last_name'=>'required',
            'email'=>'required'
        ]);

        $record = HealthDrug::find($id);
        $record->first_name =  $request->get('first_name');
        $record->last_name = $request->get('last_name');
        $record->email = $request->get('email');
        $record->job_title = $request->get('job_title');
        $record->city = $request->get('city');
        $record->country = $request->get('country');
        $record->save();

        return redirect('/medcine')->with('success', 'HealthDrug updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $record = HealthDrug::find($id);

        $record->delete();

        return redirect('/medcine')->with('success', 'HealthDrug deleted!');
    }
}
