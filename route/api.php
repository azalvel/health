<?php

use Illuminate\Http\Request;

Route::apiResource('catalog', 'BoutiqueController');
Route::apiResource('medcine', 'PharmacyController');
Route::apiResource('peoples', 'ContactsController');

